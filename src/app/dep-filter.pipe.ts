import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'depFilter'
})
export class DepFilterPipe implements PipeTransform {

  transform(searchResults, depAirport): any {
    return depAirport ? searchResults.filter(bag => bag.flight.departureAirport === depAirport) : searchResults;;
  }

}