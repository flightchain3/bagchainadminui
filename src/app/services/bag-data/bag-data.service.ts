import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class BagDataService {
  bagHistory: any = [];
  isVisible: boolean;
  constructor() { }

  setBagResult(bag:any){
  this.bagHistory = bag;
  }

  getBagResult(){
    return this.bagHistory;
  }
}
