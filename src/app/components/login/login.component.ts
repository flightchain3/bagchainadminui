import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  jwt="";
  username="saa";
  password="saaPassw0rd!";
  enterUsername="";
  enterPassword="";
  errMsg="";
  hasError = false;
  

  constructor( private loginS: LoginService) { }

  login(){
    console.log('Calliing login() function');
    this.loginS.login(this.enterUsername,this.enterPassword)
    .subscribe((res: any) => {
      console.log(res);
      this.jwt=res.jwt;
      },
      (err) => {
        console.log(err);
        this.hasError = true;
        this.errMsg="Failed to Login. Re-enter username and password.";

      });
  }

  getJWT() {
    console.log('Calliing getJWT() function');

    this.loginS.login(this.username,this.password)
    .subscribe((res: any) => {
      console.log(res);
      this.jwt=res.jwt;
      }, 
      (err) => {
        console.log(err);
        this.errMsg=err;
      });
    }

  ngOnInit() {
  }

}
