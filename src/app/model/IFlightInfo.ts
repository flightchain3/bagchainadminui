export interface IFlightInfo {
    carrier?: string;
    flightNumber?: string;
    dateOfOperation?: string;
    destination?: string;
    origin?: string;
}
