import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ILoginResponse } from './ILoginResponse';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {


  constructor(private http: HttpClient, private router: Router) {}

  login(userName: string, pass: string): Observable<ILoginResponse> {
    const url = environment.authURL;
    const httOptions = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      })
    };
    const req = {
      username:userName,
      password:pass
    };
    
    return this.http.post<ILoginResponse>(url, JSON.stringify(req), httOptions)
        .pipe(catchError(this.handleError),
         map(user =>{
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.router.navigate(['/live']);
          return user;
        }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('date');
    localStorage.removeItem('event');
    localStorage.removeItem('airline');
    localStorage.removeItem('arrAirport');
    localStorage.removeItem('destAirport');
    this.router.navigate(['/login'])
    .then(() => {
      window.location.reload();
    });
}
   
  private handleError(error: HttpErrorResponse){
    if (error.error instanceof ErrorEvent){
        console.error('An error occurred: ', error.error.message);
    } else {
        console.error('Backend returned error code: ' + error.status)
    }
    return throwError(error);
  }

}
