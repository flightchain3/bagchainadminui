import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BagHelperService {

  constructor() { }

  formatBagData(bags: any[]) {
    for (let bag of bags) {
      
        if (!bag.flight.flightNumber) {
          bag.flight.flightNumber = "N/A";
        } else {
          if(bag.flight.flightNumber.length >= 6){
            bag.flight.flightNumber = bag.flight.flightNumber;
          } else {
          bag.flight.flightNumber = bag.flight.carrierCode + bag.flight.flightNumber;
           }
        }

        if (!bag.flight.departureAirport) {
          bag.flight.departureAirport = "N/A";
        }

        if (!bag.flight.destinationAirport) {
          bag.flight.destinationAirport = "N/A";
        }

        if (!bag.flight.departureTime) {
          bag.flight.departureTime = "N/A";
        }

      if (bag.events) {
        if (!bag.events.eventName) {
          bag.events.eventName = "N/A";
        }
      } else {
        bag.events = { eventName: "N/A" }
      }

      if (bag.actions) {
        if (!bag.actions.actionName) {
          bag.actions.actionName = "N/A";
        }
      } else {
        bag.actions = { actionName: "N/A" };
      }

      if (bag.user) {
        if (!bag.user.fullName) {
          bag.user.fullName = "N/A";
        }
        if (!bag.user.type) {
          bag.user.type = "N/A";
        }
        if (!bag.user.company) {
          bag.user.company = "N/A";
        }
      } else {
        bag.user = { fullName: "N/A",
                     type: "N/A",
                     company: "N/A" };
      }

      

    }
  }
}
