import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import * as moment from 'moment';
import { BagDataService } from 'src/app/services/bag-data/bag-data.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { BagHelperService } from 'src/app/services/bagHelper/bag-helper.service';
import { Sort } from '@angular/material';
import { timestamp } from 'rxjs/operators';

@Component({
  selector: 'app-bag',
  templateUrl: './bag.component.html',
  styleUrls: ['./bag.component.less'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class BagComponent implements OnInit {
  version = "";
  noData = "N/A";
  showFlightInfo: boolean = false;
  showActionEventsInfo: boolean = false;
  bagHistory: any = [];
  searchCriteria = '';
  searchCriteriaDate = moment().format("YYYY-MM-DD");
  hasError = false;
  errMsg = "";
  sortedData = this.bagHistory;
  timestamps: any = [];

  constructor(private api: ApiService,
    private bagResult: BagDataService,
    private helper: BagHelperService) {

    this.sortedData = this.bagHistory.slice();
  }

  sortData(sort: Sort) {
    const data = this.bagHistory.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'timestamp': return this.compare(a.timestamp, b.timestamp, isAsc);
        default: return 0;
      }
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }


  ngOnInit() {
    this.bagResult.getBagResult();
    if (this.bagResult.getBagResult().length == 0) {
      this.hasError = true;
      this.errMsg = "No bag detail found. Please select a different bag.";
    }

    else {
      this.getBagHistory();
      this.sortedData = this.bagHistory;
    }
  }

  formatDate(date) {
    let day = date.getDate()
    let month = date.getMonth() + 1;
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let ampm = hours >= 12 ? 'pm' : 'am';
    day = day < 10 ? '0' + day : day;
    month = month < 10 ? '0' + month : month;
    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    let time = hours + ':' + minutes+' '+ampm;
   return date.getFullYear()+ "/" + month + "/" + day + " " + time;
  }

  getBagHistory() {
    this.bagHistory = [];
    const selectedBag = JSON.parse(this.bagResult.getBagResult());
    this.api.getBagHistory(selectedBag.bagTag, selectedBag.flight.departureDate).subscribe((res: any) => {
      for (let bag of res) {
        const bagObject = [bag.bagData];
        this.helper.formatBagData(bagObject)
        bag.timestamp = new Date(bag.timestamp);
        bag.timestamp = this.formatDate(bag.timestamp);

        this.bagHistory.push(bag);
      };


    }, (err) => {
      console.log(err);
      this.hasError = true;
      if (err.status === 401) {
        this.errMsg = "Session Expired. Redirecting to Login Screen";
        localStorage.removeItem('currentUser');
        setTimeout(() => {
          location.reload();
        }, 3000);
      }
      else {
        this.errMsg = "No bags found. Please try again.";
      }
    });
  }

}
