import { Component, OnInit } from '@angular/core';
import { SocketService } from '../../../services/socket/socket.service';
import { formatDate } from '@angular/common';
import { Router } from '@angular/router';
import { BagDataService } from 'src/app/services/bag-data/bag-data.service';
import { BagHelperService } from 'src/app/services/bagHelper/bag-helper.service';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.less']
})
export class LiveComponent implements OnInit {
  airports = require("../../../../assets/data/airports.json");
  airlines = require("../../../../assets/data/airlines.json");
  selectedAirline= "";
  selectedAirport = "";
  txType = "bagUpdate";
  iataCode = '';
  msgs: any[] = [];
  hasError = false;
  errMsg = '';

  constructor(private socketService: SocketService,
    private router: Router,
    private bag: BagDataService,
    private helper: BagHelperService) { }

  ngOnInit() {
    this.airlines.sort((a,b) => a.airlineName.localeCompare(b.airlineName));
    this.airports.sort((a,b) => a.airportName.localeCompare(b.airportName));
    this.subscribe();
  }
  ngOnDestroy() {
    this.unSubscribe();
  }



  subscribe() {  
    this.socketService.on(this.txType).subscribe((data) => {
      this.msgs.push(data);
      let count = 0;
      for(let msg of this.msgs){
      count ++;
      console.log(count +". "+JSON.stringify(msg))
      }
      this.helper.formatBagData(this.msgs);
    }, (err) => {
      console.log(err);
      if (err.status === 401) {
        this.hasError = true;
        this.errMsg = "Session Expired. Redirecting to Login Screen";
        localStorage.removeItem('currentUser');
        setTimeout(() => {
          location.reload();
        }, 3000);
      }
      else {
         this.hasError = false;
      }
    });
    this.socketService.connect(this.iataCode);
  }

  unSubscribe() {
    this.socketService.disconnect();
  }

  formatAsTime(dateTime: string) {
    if (dateTime != null) {
      return formatDate(dateTime, 'shortTime', 'en-US');
    }
    return '';
  }

  onRowClicked(bag: any) {
    this.bag.setBagResult(JSON.stringify(bag));
    this.router.navigate(['/bagHistory']);
  }

  onAirlineSelected(){
    this.unSubscribe();
    this.iataCode = this.selectedAirline;
    this.subscribe();
  }

  onAirportSelected(){
    this.unSubscribe();
    this.iataCode = this.selectedAirport;
    this.subscribe();
  }


}
