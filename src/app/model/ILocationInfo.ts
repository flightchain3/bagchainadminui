export interface ILocationInfo {
    description: string;
    geocoordinates?: string;
}
