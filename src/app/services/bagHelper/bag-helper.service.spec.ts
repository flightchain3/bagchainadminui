import { TestBed } from '@angular/core/testing';

import { BagHelperService } from './bag-helper.service';

describe('BagHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BagHelperService = TestBed.get(BagHelperService);
    expect(service).toBeTruthy();
  });
});
