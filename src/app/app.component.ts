import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LoginService } from './services/login/login.service';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']

})
export class AppComponent {

  version = environment.version;

  constructor(private log: LoginService) {}
  ngOnInit() {
  }

  logout() {
    this.log.logout();
  }



}

