export interface IPaxInfo {
    surname?: string;
    firstname?: string;
    title?: string;
    seatNumber?: string;
    status?: string;
    seqNum?: string;
}
