import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BagSearchComponent } from './bag-search.component';
import { HeaderComponent } from '../../header/header/header.component';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatCardModule,
  MatOptionModule,
  MatSelectModule,
  MatNativeDateModule
} from '@angular/material';

describe('BagSearchComponent', () => {
  let component: BagSearchComponent;
  let fixture: ComponentFixture<BagSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatCardModule,
        MatOptionModule,
        MatSelectModule,
        MatNativeDateModule,
        Ng2SearchPipeModule,
        HttpClientTestingModule,
        RouterTestingModule],
      declarations: [BagSearchComponent,
        HeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BagSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
