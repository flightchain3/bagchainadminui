import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';


@Injectable({
  providedIn: 'root'
})
export class SocketService {

  constructor(private socket: Socket) {
  }

  connect(iataCode: string): void {
    this.socket.connect();

    console.log('subscribing to ' + iataCode);
    this.socket.emit('subscribe',
      { name: 'ng subscriber', iataCode },
      (ackMsg) => { console.log('received ack:' + ackMsg); });
  }

  disconnect(): void {
    this.socket.disconnect();
    console.log('disconnected');
  }

  emit(eventType: string, payload: any, ack: (...arg: any[]) => void): void {
    this.socket.emit(eventType, payload, ack);
  }

  on(eventType: string) {
    this.socket.removeAllListeners();
    console.log('subscribing for messages');
    return this.socket
      .fromEvent<any>(eventType);
  }
}
