import { TestBed } from '@angular/core/testing';

import { BagDataService } from './bag-data.service';

describe('BagDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BagDataService = TestBed.get(BagDataService);
    expect(service).toBeTruthy();
  });
});
