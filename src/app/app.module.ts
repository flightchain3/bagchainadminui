import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatDatepickerModule,
  MatInputModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatCardModule,
  MatButtonModule,
  MatTableModule,
  MatSelectModule
} from '@angular/material';
import {
  NavbarModule,
  WavesModule,
  ButtonsModule,
  IconsModule
} from 'angular-bootstrap-md'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiService } from './services/api/api.service';
import { BagComponent } from './components/bag/bag.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './_helpers/auth.guard';
import { LoginService } from './services/login/login.service';
import { CommonModule } from '@angular/common';
import { MyMaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatListModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material';
import { MatSortModule } from '@angular/material';
import { MatPaginatorModule } from '@angular/material';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { BagSearchComponent } from './components/bagSearch/bag-search/bag-search.component';
import { HeaderComponent } from './components/header/header/header.component';
import { LiveComponent } from './components/live/live/live.component';
import { DestFilterPipe } from './dest-filter.pipe';
import { DepFilterPipe } from './dep-filter.pipe'

const config: SocketIoConfig = { url: 'wss://block-chain-event-listener-demo.blockchainsandbox.aero',
  options: { autoConnect: false, transports: ['websocket']} };

 /*
const config: SocketIoConfig = { url: 'http://localhost:8097',
  options: { autoConnect: false} };
*/ 

@NgModule({
  declarations: [
    AppComponent,
    BagComponent,
    LoginComponent,
    BagSearchComponent,
    HeaderComponent,
    LiveComponent,
    DestFilterPipe,
    DepFilterPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    NavbarModule,
    WavesModule,
    ButtonsModule,
    IconsModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    NgxMaterialTimepickerModule,
    NgbModule,
    SocketIoModule.forRoot(config),
    MyMaterialModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatExpansionModule,
    MatListModule,
    ScrollDispatchModule
  ],
  providers: [AuthGuard, LoginService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
