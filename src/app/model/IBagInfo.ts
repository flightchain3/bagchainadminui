import { IActionInfo } from './IActionInfo';
import { IEventInfo } from './IEventInfo';
import { IFlightInfo } from './IFlightInfo';
import { ILocationInfo } from './ILocationInfo';
import { IPaxInfo } from './IPaxInfo';

export interface IBagInfo {
    bagTag: string;
    locationInfo: ILocationInfo;
    operatorID?: string;
    flight?: IFlightInfo;
    passengers?: IPaxInfo[];
    events?: IEventInfo;
    actions?: IActionInfo;
}
