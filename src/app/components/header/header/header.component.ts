import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  title = 'Bagchain Admin UI';
  value = 'Search'
  link = '/bagSearch';
  constructor(private log: LoginService, private router: Router) { }

  ngOnInit() {
    if(this.router.url == '/bagSearch'){
      this.value = 'Live';
      this.link = '/live';
    } else {
      this.value = 'Search';
      this.link = '/bagSearch';
    }
  }

  logout(){
   this.log.logout();
  }

}
