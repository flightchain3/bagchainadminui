import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { LoginService } from 'src/app/services/login/login.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {
  MatDatepickerModule,
  MatInputModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatCardModule,
  MatButtonModule,
  MatTableModule
} from '@angular/material';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  const mockLoginService = { login: jasmine.createSpy('mockLoginSpy') }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        MatDatepickerModule,
        MatInputModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatCardModule,
        MatButtonModule,
        MatTableModule,
        BrowserAnimationsModule
      ],
      declarations: [LoginComponent],
      providers: [
        { provide: LoginService, useValue: mockLoginService }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('login', () => {
    it('should call getJWT()', () => {
      mockLoginService.login.and.returnValue(of(new Promise((resolve) => { resolve(); })));
      component.username = 'saa';
      component.password = 'saaPassw0rd!';
      component.getJWT();
      expect(mockLoginService.login).toHaveBeenCalledWith('saa', 'saaPassw0rd!');
    });


    it('should show error when getJWT() call fails', () => {
      mockLoginService.login.and.returnValue(of(new Promise((resolve, reject) => { reject('failed to get jwt.'); })));

      component.username = 'user';
      component.password = 'pass';
      component.errMsg = 'failed to get jwt.';
      component.getJWT();

      setTimeout(() => {
        expect(component.errMsg).toEqual('failed to get jwt.');
      });
    });
  });
});


