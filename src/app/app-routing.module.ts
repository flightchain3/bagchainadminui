import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BagComponent } from './components/bag/bag.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './_helpers/auth.guard';
import { BagSearchComponent } from './components/bagSearch/bag-search/bag-search.component';
import { LiveComponent } from './components/live/live/live.component';


const routes: Routes = [
  { path:'',redirectTo:'/login',pathMatch:'full'},
  { path: 'login', component: LoginComponent },
  { path: 'bagHistory', component: BagComponent,canActivate: [ AuthGuard ] },
  { path: 'bagSearch', component: BagSearchComponent, canActivate: [ AuthGuard ]},
  { path: 'live', component: LiveComponent, canActivate: [ AuthGuard ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
