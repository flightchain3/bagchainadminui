export const environment = {
  production: true,
  authURL:'https://baggage-api-demo.blockchainsandbox.aero/auth/login',
  getURL:'https://baggage-api-demo.blockchainsandbox.aero:3000/bag/',
  version: require('../../package.json').version 
};
