set -e # Turn error checking back on

#Creates namespace or ignores if already there
## Using funky two stage create then apply throughout script to create if new and ignore or update if exists.
# kubectl create namespace "adapters" -o yaml --dry-run | kubectl apply -f -
echo "namespace created"

#Needed so k8 can authenticate when pulling docker image of deployment.yml
# This might be better done as a one time activity to prevent dealing with tokena
kubectl create secret docker-registry "bagchainui" --docker-server="registry.gitlab.com/flightchain3/bagchainadminui" --docker-username=$GITLAB_USERNAME --docker-password=$GITLAB_PASSWORD --docker-email=$GITLAB_EMAIL -o yaml --dry-run | kubectl apply --namespace="adapters" -f -

#For visual inspection of substitutions
cat k8s-deployments/*.yml

#apply all of the yamls in deployment folder
kubectl apply -f k8s-deployments --namespace="adapters"
echo "deployed"
