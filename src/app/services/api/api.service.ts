import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { IBagInfoHistory } from 'src/app/model/IBagInfoHistory';
import { IBagInfo } from 'src/app/model/IBagInfo';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  currentUser = JSON.parse(localStorage.getItem('currentUser'));

  constructor(private httpClient: HttpClient,private router: Router) { }
  baseUrl: string = "https://baggage-api-demo.blockchainsandbox.aero";
  query = '';

  getVersion() {
    return this.httpClient.get(this.baseUrl + '/version', { responseType: 'text' });
  }

  getEventList():Observable<any[]>{
    this.query = this.baseUrl + '/bag/events';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.currentUser.jwt
      })
    }

    return this.httpClient.get<any[]>(this.query, httpOptions)
    .pipe(catchError(this.handleError));

  }

  getBagHistory(bagKey: any, dateSearch: any): Observable<IBagInfoHistory[]> {
    if (bagKey.toString().length < 10) {
      for (let i = bagKey.toString().length; i < 10; i++) {
        bagKey = '0' + bagKey;
      }

    }
    this.query = this.baseUrl + '/bag/' + dateSearch + '/' + bagKey + '/history';


    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.currentUser.jwt
      })
    }

    return this.httpClient.get<IBagInfoHistory[]>(this.query, httpOptions)
      .pipe(catchError(this.handleError));
  }

  getBagQuery(searchQuery: any): Observable<IBagInfo[]> {

    this.query = this.baseUrl + '/bag/find/';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.currentUser.jwt
      })
    }
    const req = {
      "selector":{
        "flight.departureDate": searchQuery
      }
    };
    return this.httpClient.post<IBagInfo[]>(this.query, JSON.stringify(req), httpOptions)
    .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred: ', error.error.message);
    } else if (error.status === 401 || this.currentUser === null) {
      console.log('Unauthorized');
    } else {
      console.error('Backend returned error code: ' + error.status);
    }
    return throwError(error);
  }

}

