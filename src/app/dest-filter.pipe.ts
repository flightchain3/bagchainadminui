import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'destFilter'
})
export class DestFilterPipe implements PipeTransform {

  transform(searchResults, destAirport): any {
    return destAirport ? searchResults.filter(bag => bag.flight.destinationAirport === destAirport) : searchResults;;
  }

}