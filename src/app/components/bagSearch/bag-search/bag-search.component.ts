import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { BagDataService } from 'src/app/services/bag-data/bag-data.service';
import { BagHelperService } from 'src/app/services/bagHelper/bag-helper.service';

@Component({
  selector: 'app-bag-search',
  templateUrl: './bag-search.component.html',
  styleUrls: ['./bag-search.component.less']
})
export class BagSearchComponent implements OnInit {
  airports = require("../../../../assets/data/airports.json");
  airlines = require("../../../../assets/data/airlines.json");
  searchCriteriaDate = localStorage.getItem("date") || moment().format("YYYY-MM-DD") 
  searchResult: any = [];
  tableData: any = [];
  eventList: any = [];
  noData = "N/A";
  searchText: string;
  eventType = localStorage.getItem("event") || "";
  selectedAirline= localStorage.getItem("airline") ||"";
  depAirport = localStorage.getItem("arrAirport") ||"";
  destAirport = localStorage.getItem("destAirport") || "";
  hasError = false;
  errMsg = "";

  constructor(private api: ApiService,
    private router: Router,
    private bag: BagDataService,
    private helper: BagHelperService ) { 
    }

  ngOnInit() {
    this.getBagQuery();
    this.getEventListing();

    this.airlines.sort((a,b) => a.airlineName.localeCompare(b.airlineName));
    this.airports.sort((a,b) => a.airportName.localeCompare(b.airportName));
  }

  getEventListing(){
    this.api.getEventList().subscribe((res: any) =>{
    this.eventList = res;
    this.eventList.sort((a,b) => a.description.localeCompare(b.description));

    });
  }

  getBagQuery() {
    this.searchResult = [];
    const momentDate = new Date(this.searchCriteriaDate);
    const formattedDate = moment(momentDate).format("YYYY-MM-DD");
    localStorage.setItem("date",formattedDate);
    this.api.getBagQuery(formattedDate)
      .subscribe((res: any) => {
        
        for(let bag of res){
        this.searchResult.push(bag.bagData);
        }
  
        this.helper.formatBagData(this.searchResult);

        if(this.searchResult.length == 0) {
          this.hasError = true;
          this.errMsg = "No bags found. Please try a different date.";
        } else {
          this.hasError = false;
        }
      },
        (err) => {
          console.log(err);
          if (err.status === 401) {
            this.searchResult = null;
            this.hasError=true;
            this.errMsg = "Session Expired. Redirecting to Login Screen";
            localStorage.removeItem('currentUser');
            setTimeout(() => {
              location.reload();
            }, 3000);
          }
    

        });
  }

  onRowClicked(bag: any) {
    console.log(this.bag);
    this.bag.setBagResult(JSON.stringify(bag));
    this.router.navigate(['/bagHistory']);
  }

  onEventSelected(){
    localStorage.setItem("event", this.eventType);
  }

  onDepSelected(){
    localStorage.setItem("depAirport", this.depAirport);
  }
  onDestSelected(){
    localStorage.setItem("destAirport", this.destAirport);
  }
  onAirlineSelected(){
    localStorage.setItem("airline", this.selectedAirline);
  }
}
